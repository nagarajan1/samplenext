import Style from '@/styles/MainLayout.module.scss'
import SideMenubar from '@/components/SideMenubar'
import CustomHeader from '@/components/Header';
import CustomFooter from '@/components/Footer';
import Sidebar from '@/components/Sidebar';

const MainLayout = ({children}) => {
    return (
        <div className={Style.mainLayout}>
            <div>
            <SideMenubar />
            <Sidebar/>
            </div>
            <div className={Style.mainBody}>
                <CustomHeader />
                <main className={`${Style.Page_layout}`}>
                    <div className={`${Style.Page_content} fixed`}>
                        {children}
                    </div>
                </main>
                <CustomFooter />
            </div>
        </div>
    );
};

export default MainLayout;