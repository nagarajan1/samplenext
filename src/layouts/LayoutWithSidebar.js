import React, { useState }  from 'react';
import CustomFooter from '@/components/footer';
import CustomHeader from '@/components/header';
import Sidebar from '@/components/sidebar'
import Style from '@/styles/LayoutWithSidebar.module.scss'

const LayoutWithSidebar = ({ children }) => {
  const[closeBar,setCloseBar] = useState(true)
  const collapseClose = () =>{
    setCloseBar(!closeBar)
  }
    return (
        <div>
            <CustomHeader />
            <main className={`${Style.main_layout}`}>
                <Sidebar expandbar={closeBar} Close={collapseClose}/>
                <div className={`${Style.main_body} ${closeBar ? "expandsidebar" : ""} fixed`}>
                    <div className={`${Style.main_inner_body} fixed`}>
                        {children}
                    </div>
                </div>
            </main>
            <CustomFooter />
        </div>
    );
};

export default LayoutWithSidebar;