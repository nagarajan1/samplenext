import React, { useEffect } from 'react';
import CustomFooter from '../components/Footer';
import CustomHeader from '../components/header';
import Style from '../styles/LoginLayout.module.scss'

const LoginLayout = ({ children }) => {
    return (
        <div>
            <CustomHeader className={`${Style.IC_Login}`}/>
            <main className={`${Style.main_layout}`}>
                <div className={`${Style.main_body} fixed`}>
                    {children}
                    {/* </div> */}
                </div>
            </main>
            <CustomFooter />
        </div>
    );
};

export default LoginLayout;