import CustomFooter from '@/components/Footer';
import CustomHeader from '@/components/Header';
import Sidebar from '@/components/sidebar'
import React from 'react';
import Style from '@/styles/MainBlogLayout.module.scss'

const MainLayout = ({ children }) => {
    return (
        <div>
            <CustomHeader />
            <Sidebar/>
            <main className={`${Style.main_layout}`}>
                <div className={`${Style.main_body} fixed`}>
                    {children}
                </div>
            </main>
            <CustomFooter />
        </div>
    );
};

export default MainLayout;