import Image from 'next/image';
import React, { useState } from 'react';
import ToggleON from '@/assets/icons/toggle_on.svg';
import ToggleOFF from '@/assets/icons/toggle_off.svg';

const CustomToggle = ({onStatus,setOnStatus}) => {
  
  return (
    <div className="form_toggle_input">
      <Image 
        src={ onStatus ? ToggleON : ToggleOFF } 
        width={52} 
        height={24}
        onClick={() => setOnStatus((prev) => !prev)}
        alt={ onStatus ? 'Toggle ON' : 'Toggle OFF' }
        className={ onStatus ? 'on' : 'off' }
      />
    </div>
  );
};

export default CustomToggle;