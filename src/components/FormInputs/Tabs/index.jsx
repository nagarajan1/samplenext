import React,{useState} from "react";
import style from "./tabs.module.scss"

const CustomTabs = ({tabs}) => {
  const [activeTab, setActiveTab] = useState(tabs[0].id);

  const handleTabClick = (id) => {
    setActiveTab(id);
  };

  return (
    <div className={style.tabs_form}>
      <div className="tab-list">
        {tabs.map((tab) => (
          <div
            key={tab.id}
            onClick={() => handleTabClick(tab.id)}
            className={`tab ${activeTab === tab.id ? "active" : ""}`}
          >{tab.label}</div>
        ))}
      </div>
      <div className="tab-content">
        {tabs.find((tab) => tab.id === activeTab)?.content}
      </div>
    </div>
  );
};
export default CustomTabs;
