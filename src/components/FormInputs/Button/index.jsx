import React from "react";
import style from "./button.module.scss";
import { Colors } from "@/utils/colors";
import Spin from "../../../assets/icons/spinner.svg";
import Image from "next/image";

const Button = ({
  name,
  loading,
  className,
  type,
  onClick,
  nameStyle,
  size,
  bg,
  width,
  ...rest
}) => {
  console.log(rest);
  return (
    <div className={style.form_btn}>
      <button
        type={type}
        className={`btn f-16  ${className} ${loading ? "load-pad" : ""} ${
          size === "xlg"
            ? "sz-xlg"
            : size === "lrg-18"
            ? "sz-lrg-18"
            : size === "lrg-16"
            ? "sz-lrg-16"
            : size === "med-16"
            ? "sz-med-16"
            : size === "med-14"
            ? "sz-med-14"
            : size === "sml-14"
            ? "sz-sml-14"
            : size === "sml-12"
            ? "sz-sml-12"
            : ""
        }`}
        disabled={loading}
        onClick={onClick}
        {...rest}
        style={{
          backgroundColor: bg ? Colors[bg] : Colors.primary700,
          color: rest.color ? Colors[rest.color] : Colors.white,
          fontWeight:rest.fontWeight
        }}
      >
        {!loading && <span className={`${nameStyle}`}>{name}</span>}
        {loading && <Image src={Spin} alt="spinner" width={40} height={40} />}
      </button>
    </div>
  );
};

export default Button;
