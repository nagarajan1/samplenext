import { useState } from "react";
import Style from './customtabs.module.scss'

const Custom_Tabs = ({ tabsList, whichTab,changeTab }) => {
    return (
        <div className={Style.CustomTabs}>
            <ul className="nav nav-tabs custom-ul justify-content-center">
                {tabsList.map((item, i) => (
                    <li key={i} className="nav-item">
                        <button
                            className={`nav-link ${whichTab === item ? "active" : null
                                }`}
                            onClick={() => changeTab(item)}
                        >
                            {item}
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default Custom_Tabs;