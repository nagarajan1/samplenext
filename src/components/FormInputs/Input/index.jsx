import React, { useState, useRef } from "react";
import style from "./input.module.scss";
import Image from "next/image";
import copyIcon from "@/assets/icons/input_copy_svg.svg";
import editIcon from "@/assets/icons/info_icon_blue.svg";

const CustomInput = ({
  type,
  copy,
  edit,
  placeholder,
  onChange,
  className,
  ...rest
}) => {
  const [inputEdit, setInputEdit] = useState(true);
  const inputRef = useRef(null);

  //   Logic for copying the input text
  const handleCopyClick = async () => {
    if (inputRef.current) {
      inputRef.current.select();
      try {
        await navigator.clipboard.writeText(inputRef.current.value);
      } catch (err) {
        console.error("Error copying text: ", err);
      }
    }
  };

  return (
    <div className={`${style.form_input} ${className}`} style={{display:edit || copy &&"flex" }}>
      <span style={{ position: "relative",display:"flex"}}>
        <input
          type={type}
          className={`input`}
          ref={inputRef}
          disabled={edit?inputEdit:false}
          placeholder={placeholder}
          onChange={onChange}
          {...rest}
        />
        {edit && (
          <span
            className="form_input_edit_icon"
            onClick={() => setInputEdit(false)}
          >
            Edit
            <Image src={editIcon} />
          </span>
        )}
         {copy && (
        <span className="form_input_copy_icon" onClick={handleCopyClick}>
          <Image src={copyIcon} />
        </span>
      )}
      </span>

     
    </div>
  );
};

export default CustomInput;
