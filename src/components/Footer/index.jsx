import React from 'react';
import Style from './CustomFooter.module.scss'
import Image from 'next/image'
import Logo from '@/assets/images/ic-logo-footer.svg'

const CustomFooter = () => {
    return (
        <div className={`${Style.Customfooter} bg-primary-150 text-center p-3 d-flex justify-content-center`}>
            {/* <Image className='me-3' src={Logo} alt='' width={108} height={25}/> */}
            <span className={`text-primary-500 ms-1 ${Style.copyText}`}>
                {" "}
                © 2022 Lighterside of Real Estate -
                <a className={`text-primary-500 ms-1 text-decoration-underline ${Style.copyText}`} href={'/privacy-policy'}>
                    Privacy{" "}
                </a>
                -{" "}
                <a className={`text-primary-500 text-decoration-underline ${Style.copyText}`} href={'/terms'}>
                    Terms
                </a>
            </span>
        </div>
    );
};

export default CustomFooter;