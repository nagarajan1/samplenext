import React from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import Style from './CustomHeader.module.scss'
import Image from 'next/image'
import SunLogo from '@/assets/images/LSORE-sun.svg'
const CustomHeader = ({ className }) => {
    return (
        <Navbar bg="light" expand="lg" className={`${Style.Cus_Nav} bg-primary-700 ${className}`}>
            <Container fluid className={`${Style.Cus_inner}`}>
                <Navbar.Toggle aria-controls="basic-navbar-nav" className='custom_toggle'/>
                <Navbar.Brand className='brand_Image' href="/">
                    <Image src={SunLogo} alt="" width={42} height={44} />
                </Navbar.Brand>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default CustomHeader;