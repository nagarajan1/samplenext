import React from 'react';
import Styles from './SideMenubar.module.scss'
import Image from 'next/image'
import SunLogo from '@/assets/images/LSORE-sun.svg'

const Menubar = () => {
    return (
        <div className={`${Styles.menuBar} bg-primary-700`}>
            <Image className={`cursor-point`}  src={SunLogo} alt="" width={42} height={44} />
        </div>
    );
};

export default Menubar;