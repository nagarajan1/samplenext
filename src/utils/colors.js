export const Colors={
    primary800   : "#031A26",
    primary700   : "#2C404E",
    primary650   : "#3A4F61",      
    primary600   : "#576C80",
    primary500   : "#849AAE",      /*Toggle off, Tertiary button*/
    primary400   : "#B2C2D0",      /*Crossed out text (checkout),Placeholder in text field*/
    primary300   : "#CEDAE5",      /*Horizontal rows,Filter sidebar title sections bkd*/
    primary200   : "#DFE7EF",      /*Section background, Text field outlines,Expanded left-drawer menu bkd*/
    primary150   : "#E6EDF4",      /*Primary box slight darker  selection*/
    primary100   : "#F0F5F9",      /*Primary box bkd, Unselected button, Category filter menu bkd, Manage pages leftside drawer menu, Default sidebar bkd, Dropdown menu highlight*/
    primary50    : "#F5F8FA",      /*Calendar day-highlight bkd*/
    action700    : "#156CBD",
    action500    : "#48A0F3",      /*Primary action button color, Hyperlink text, Info icon, Shared meme checkmark icon bkd*/
    action300    : "#A4D3FF",
    action100    : "#DDEFFF",      /*Notification box bkd */
    attention700 : "#982805",
    attention500 : "#FF6756",      /* Primary red action, Error text, Alert info icon, Archived article icon bkd */
    attention300 : "#FFBCB2",
    attention100 : "#FFE8E5",      /* Error box bkd */
    success500   : "#38AF00",      /* Success text */
    success100   : "#E7FFCF",      /* Success box bkd */
    notice500    : "#FFDD6D",      /* Preview btn */
    notice100    : "#FFF5D4",      /* Notice box bkd */
    toggleOn     : "#87D23A",      /* toggleOn events */
    white        : "#FFF",
    black        : "#000",
}

export const FontSizes={
    large:'32px',
    medium:'24px',
    regular:'16px'
}