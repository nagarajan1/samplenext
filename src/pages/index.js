import Button from "@/components/formInputs/button";
import Image from "next/image";
// import { Inter } from '@next/font/google'
import styles from "@/styles/Home.module.scss";
import LoginLayout from "@/layouts/LoginLayout";
import { Container, Row, Col, Card, Form, Input } from "react-bootstrap";
import { List } from "@/constants/commandList";
import Emoji from "@/assets/icons/emoji.svg";
import React, { useEffect } from "react";
import CustomInput from "@/components/FormInputs/Input";
import loginLogo from "@/assets/images/logo_circle.svg";

// const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  const fillImg =
    process.env.NEXT_PUBLIC_CDNURL + "uploads/2022/05/login_bg.jpeg";
  const bannerImage = {
    backgroundImage: "url(" + fillImg + ")",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundAttachment: "fixed",
  };
  return (
    <>
      {/* banner section start */}
      <div className={`${styles.Ic_login_main}`} style={bannerImage}>
        <Container className="Login_container">
          <div className="Inner-grid">
            <Row>
              <Col md={{ order: 1, span: 8 }} xs={{ order: 2, span: 12 }}>
                <div className="d-flex justify-content-center">
                  <div>
                    <p className="text-white mt-4 mb-4">
                      What is the Lighter Side’s Inner Circle?
                    </p>
                    <h1 className="text-white fw-700 text-start">
                      Personalized social media content that keeps agents top of
                      mind.
                    </h1>
                    <div className="login-more">
                      <Button
                        size="lrg-16"
                        name="Learn More"
                        bg="notice500"
                        color="primary700"
                        fontWeight="700"
                        width="140"
                      ></Button>
                    </div>
                  </div>
                </div>
              </Col>
              <Col
                md={{ order: 2, span: 4 }}
                xs={{ order: 1, span: 12 }}
                className="align-self-center res-center"
              >
                {/* <Card className="login_form">
                  <Image src={loginLogo} />
                  <h5>Member sign in</h5>
                  <Card.Body >
                    <form>
                      <CustomInput type="text" placeholder="Username" className="border-0"/>
                      <CustomInput
                        type="password"
                        placeholder="Password"
                        className="mt-2 border-0"
                      />

                      <Row className="d-flex justify-content-between">
                        <Col xs={6} lg={6} md={12} className="password_sensitive">
                          <p>(Passwords are case-sensitive)</p>
                        </Col>
                        <Col xs={6} lg={6} md={12} className="forgot_password">
                          <a href="#">Forgot Password?</a>
                        </Col>
                      </Row>
                      <Row className="text-center mt-3">
                        <Button size="med-16" name="Sign In" bg="action500" />
                      </Row>
                      <div className="d-flex justify-content-center mt-2">
                      <input
                        type="checkbox"
                      />
                      <label for="vehicle2" className="ms-2">Remember me</label>
                      </div>
                    </form>
                  </Card.Body>
                </Card> */}
                <Card className="login_Card br-5">
                  <div className="Ic-logo">
                    <Image src={loginLogo} />
                  </div>
                  <Card.Body className="Card-Content">
                    <h5 className="Bodylarge fw-700">Member sign in</h5>
                    <Form className="login-form" noValidate autoComplete="off">
                      <Row>
                        <Col lg="12">
                          <CustomInput
                            type="text"
                            name="email"
                            size="lg"
                            placeholder="Username"
                            className="mb-0"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="12">
                          <CustomInput
                            required
                            type="password"
                            name="password"
                            size="lg mb-0"
                            placeholder="Password"
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col lg="12" className="frgt-link">
                          <p className="f-12 text-primary-500 mb-0 mt-1 position-absolute pwd-info">
                            (Passwords are case-sensitive)
                          </p>
                          <a
                            className="reset-link-txt text-action-500 f-12 fw-400"
                            style={{ textDecoration: "none" }}
                            type="button"
                          >
                            Forgot Password?
                          </a>
                        </Col>
                      </Row>
                      <div className={`text-center mt-3`}>
                        <Button
                          name="Sign In"
                          type="submit"
                          className={`text-white login-btn`}
                          nameStyle="f-16 lh-24 fw-400"
                        />
                      </div>
                      <Row>
                        <Col lg="12" className="text-center">
                          <div className="checkbox mt-2 f-12">
                            <Form.Check
                              type={"checkbox"}
                              label={`Remember me`}
                              defaultChecked
                              className="checkbox text-primary-700 remember"
                            />
                          </div>
                        </Col>
                      </Row>
                    </Form>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
      {/* banner section end */}

      {/* command Image section start */}
      <div className={styles.CmdList}>
        <Container className="Login_container">
          <div className="Ic-image-content">
            <h2 className="text-center text-primary-700 f-36 lh-38 fw-700">
              Wouldn’t you <span className="fs-i">love</span> your sphere to say
              things like this about you?
            </h2>
            <p className="text-center mt-2">
              <b className=" f-20 lh-26 text-primary-700">
                Our members get love like this all the time.{" "}
                <Image
                  className="emoji"
                  src={Emoji}
                  alt=""
                  width={20}
                  height={20}
                />
              </b>
            </p>
            <div className="card-columns text-center">
              {List.map((item, i) => (
                <img
                  className="boxshadow"
                  src={process.env.NEXT_PUBLIC_CDNURL + item.img}
                  key={i}
                  alt=""
                />
              ))}
            </div>
          </div>
        </Container>
      </div>
      {/* command Image section end */}

      {/* middle footer start */}
      <div className={`${styles.LearnMore} bg-action-500`}>
        <Container className="Login_container">
          <Row>
            <Col lg="8" sm="8" className="p-0">
              <p className="text-white mb-0 fw-700">
                We didn’t invent top-of-mind marketing for real estate agents.
                We perfected it.
              </p>
            </Col>
            <Col
              lg="4"
              sm="4"
              className="align-self-center content-middle-button"
            >
              <div className="login-more text-center">
                <Button
                  name="Learn More"
                  type="submit"
                  className="btn-notice-500"
                  nameStyle="fw-700 text-primary-700"
                  size="lg"
                  // onClick={funnelRedirect}
                />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
      {/* middle footer end */}
    </>
  );
}

Home.getLayout = function getLayout(page) {
  return <LoginLayout>{page}</LoginLayout>;
};
