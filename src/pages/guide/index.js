import Header from '@/components/Header'
import Footer from '@/components/Footer'
import Button from '@/components/FormInputs/Button'
import CustomToggle from '@/components/FormInputs/Toggle'
import CustomInput from '@/components/FormInputs/Input'
import { useState } from 'react'
import CustomTabs from '@/components/FormInputs/Tabs'
import Custom_Tabs from '@/components/FormInputs/CustomTabs'

export default function Guide() {

  const tabs = [
    {
      id: "tab1",
      label: "Tab 1",
      content: <div>Content for Tab 1</div>,
    },
    {
      id: "tab2",
      label: "Tab 2",
      content: <div>Content for Tab 2</div>,
    },
    {
      id: "tab3",
      label: "Tab 3",
      content: <div>Content for Tab 3</div>,
    },
  ];
  const onMouseEnter = () => {
      console.log("click")
  }

  // for Toggle
  const [onStatus, setOnStatus] = useState(false);
  const [onStatustwo, setOnStatustwo] = useState(true);
  console.log(onStatus,onStatustwo);
  const ceckstate= () =>{
    console.log("first")
  }

  const tabsName=["General","LeadGen","CustomColors","Blog"]

  
  const [whichTab, setWhichTab] = useState("General");
  const changeTab = (url) => {
      setWhichTab(url);
  };

  return (
    <>
    <Button size="xlg" name="click" color="white"  loading={true} />
    <Button size="lrg-18" name="click" color="green" bg="action300"/>
    <Button size="med-16" name="click" color="white" />
    <Button size="med-14" name="click" color="white" />
    <Button size="sml-14" name="click" color="white" />
    <Button size="sml-12" name="click" color="white" />
    <CustomInput type="text" copy={true} edit={false} placeholder="myname"/>
    {/* <br/>
    <CustomToggle id="ct_1" toggleHandler={()=>toggleHandler('ct1')} onStatus={onStatus} /> */}
    <CustomToggle id="ct_2" onStatus={onStatus} setOnStatus={setOnStatus} />
    <br/>
    <CustomToggle id="ct_2" 
    // onStatus={onStatustwo} 
    // setOnStatus={setOnStatustwo} 
    onClick={ceckstate}/>
    <CustomTabs tabs={tabs}/>
    <Custom_Tabs tabsList={tabsName} whichTab={whichTab} changeTab={changeTab}/>

    </>
  )
}
