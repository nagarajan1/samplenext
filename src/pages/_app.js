import MainLayout from '@/layouts/MainLayout'
import '@/styles/globals.scss'
import '@/styles/_mixins.scss'

export default function App({ Component, pageProps }) {
  const getLayout = Component.getLayout
  if (!getLayout) {
    return (
      <MainLayout><Component {...pageProps} /></MainLayout>
    )
  } else {
    return getLayout(<Component {...pageProps} />)
  }
}
